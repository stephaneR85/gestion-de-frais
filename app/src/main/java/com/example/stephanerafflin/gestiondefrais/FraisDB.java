package com.example.stephanerafflin.gestiondefrais;

/**
 * Created by stephane.rafflin on 11/09/2017.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class FraisDB {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "GestionPro.db";

    private static final String TABLE_FRAIS = "frais";
    private static final String COL_ID = "_id";
    private static final String COL_TYPE = "type";
    private static final String COL_MONTANT = "montant";
    private static final String COL_DATE = "date";
    private SQLiteDatabase db;
    private MySQLiteDataBase maBaseSQLite;
    private static final String createTable = "CREATE TABLE "
            + TABLE_FRAIS + " (" + COL_ID
            + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " + COL_TYPE
            + " TEXT NOT NULL, " + COL_MONTANT + " DOUBLE NOT NULL, "
            + COL_DATE + " TEXT NOT NULL);";

    public FraisDB(Context context)  {
        maBaseSQLite = new MySQLiteDataBase(context, NOM_BDD, null, VERSION_BDD, createTable, TABLE_FRAIS);
    }

    public void open() {
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close() {
        db.close();
    }


    public long insertFrais(Frais frais) {
        ContentValues values = new ContentValues();
        values.put(COL_TYPE, frais.getType());
        values.put(COL_MONTANT, frais.getMontant());
        values.put(COL_DATE, frais.getDate());
        return db.insert(TABLE_FRAIS, null, values);
    }

    public Cursor getFrais(String d) {
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_FRAIS + " WHERE date LIKE ?", new String[]{"%"+" - "+d});
        return c;
    }

    public Cursor getMont(String m, String a) {
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_FRAIS + " WHERE date LIKE ?",new String[]{"%"+" - "+m+" - "+a});
        return c;
    }

    public int delete(int id) {
        return db.delete(TABLE_FRAIS, COL_ID + " = " + id, null);
    }

    public int updateFrais(int id, Frais frais) {
        ContentValues values = new ContentValues();
        values.put(COL_TYPE, frais.getType());
        values.put(COL_MONTANT, frais.getMontant());
        values.put(COL_DATE, frais.getDate());
        return db.update(TABLE_FRAIS, values, COL_ID + " = " + id, null);
    }
    public void remove(){
        //db.execSQL("DROP TABLE frais;");
    }

}