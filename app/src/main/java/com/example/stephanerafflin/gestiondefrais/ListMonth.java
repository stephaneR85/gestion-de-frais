package com.example.stephanerafflin.gestiondefrais;

import android.content.Intent;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by stephane.rafflin on 09/10/2017.
 */

public class ListMonth extends AppCompatActivity {
    String year;
    EditText annee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listmonth);
        annee = (EditText) findViewById(R.id.annee);
        if(getIntent().getExtras()==null) {
            Calendar cal = Calendar.getInstance();
            int a = cal.get(Calendar.YEAR);
            year = Integer.toString(a);
        }
        else{
            Bundle d = getIntent().getExtras();
            year = d.getString("annee");
            Toast.makeText(getApplicationContext(), year, Toast.LENGTH_LONG).show();
        }
        annee.setText(year);
        Button btnRet = (Button) findViewById(R.id.retourM);
        btnRet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListMonth.this, MainActivity.class);
                startActivity(intent);
            }
        });
        Button btnActu = (Button) findViewById(R.id.actu);
        btnActu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                year = annee.getText().toString();
                Intent intent = new Intent(ListMonth.this, ListMonth.class);
                intent.putExtra("annee",year);
                finish();
                startActivity(intent);
            }
        });

        TextView mont1 = (TextView) findViewById(R.id.montJanvier);
        float m1 = calculMois("1", year);
        mont1.setText(m1+" €");
                Button b1 = (Button) findViewById(R.id.bouton1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "1 - "+year);
                startActivity(intent);
            }
        });
        TextView mont2 = (TextView) findViewById(R.id.montFevrier);
        float m2 = calculMois("2", year);
        mont2.setText(m2 +" €");
        Button b2 = (Button) findViewById(R.id.bouton2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "2 - "+year);
                startActivity(intent);
            }
        });
        TextView mont3 = (TextView) findViewById(R.id.montMars);
        float m3 = calculMois("3", year);
        mont3.setText(m3 +" €");
        Button b3 = (Button) findViewById(R.id.bouton3);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "3 - "+year);
                startActivity(intent);
            }
        });
        TextView mont4 = (TextView) findViewById(R.id.montAvril);
        float m4 = calculMois("4", year);
        mont4.setText(m4 +" €");
        Button b4 = (Button) findViewById(R.id.bouton4);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "4 - "+year);
                startActivity(intent);
            }
        });
        TextView mont5 = (TextView) findViewById(R.id.montMai);
        float m5 = calculMois("5", year);
        mont5.setText(m5 +" €");
        Button b5 = (Button) findViewById(R.id.bouton5);
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "5 - "+year);
                startActivity(intent);
            }
        });
        TextView mont6 = (TextView) findViewById(R.id.montJuin);
        float m6 = calculMois("6", year);
        mont6.setText(m6 +" €");
        Button b6 = (Button) findViewById(R.id.bouton6);
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "6 - "+year);
                startActivity(intent);
            }
        });
        TextView mont7 = (TextView) findViewById(R.id.montJuillet);
        float m7 = calculMois("7", year);
        mont7.setText(m7 +" €");
        Button b7 = (Button) findViewById(R.id.bouton7);
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "7 - "+year);
                startActivity(intent);
            }
        });
        TextView mont8 = (TextView) findViewById(R.id.montAout);
        float m8 = calculMois("8", year);
        mont8.setText(m8 +" €");
        Button b8 = (Button) findViewById(R.id.bouton8);
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "8 - "+year);
                startActivity(intent);
            }
        });
        TextView mont9 = (TextView) findViewById(R.id.montSeptembre);
        float m9 = calculMois("9", year);
        mont9.setText(m9 +" €");
        Button b9 = (Button) findViewById(R.id.bouton9);
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "9 - "+year);
                startActivity(intent);
            }
        });
        TextView mont10 = (TextView) findViewById(R.id.montOctobre);
        float m10 = calculMois("10", year);
        mont10.setText(m10 +" €");
        Button b10 = (Button) findViewById(R.id.bouton10);
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "10 - "+year);
                startActivity(intent);
            }
        });
        TextView mont11 = (TextView) findViewById(R.id.montNovembre);
        float m11 = calculMois("11", year);
        mont11.setText(m11 +" €");
        Button b11 = (Button) findViewById(R.id.bouton11);
        b11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "11 - "+year);
                startActivity(intent);
            }
        });
        TextView mont12 = (TextView) findViewById(R.id.montDecembre);
        float m12 = calculMois("12", year);
        mont12.setText(m12 +" €");
        Button b12 = (Button) findViewById(R.id.bouton12);
        //Toast.makeText(getApplicationContext(), d+"€",Toast.LENGTH_SHORT).show();
        b12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMonth.this, HistoActivity.class);
                intent.putExtra("date", "12 - "+year);
                startActivity(intent);
            }
        });

    }
    public float calculMois(String m, String a){
        float somme = 0;
        FraisDB db = new FraisDB(this);
        db.open();
        Cursor c = db.getMont(m, a);
        try {
            if (c.moveToFirst()) {
                do {
                    float d = (float) c.getDouble(2);
                    somme = somme+d;
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No found" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        c.close();
        db.close();
        return somme;
    }
}