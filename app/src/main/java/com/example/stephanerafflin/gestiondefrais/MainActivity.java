package com.example.stephanerafflin.gestiondefrais;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button btnHisto = (Button) findViewById(R.id.btnHisto);
        btnHisto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListMonth.class);
                startActivity(intent);
            }
        });

        Button b = (Button) findViewById(R.id.SaisirDate);
        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, dateActivity.class);
                startActivity(intent);
            }
        });
        Bundle date1 = getIntent().getExtras();
        final TextView dateChoi = (TextView) findViewById(R.id.dateChoi);
        if(date1 != null){
            if(date1.getString("jour") == "null") {
                dateChoi.setText(" ");
            }
            else{
                dateChoi.setText(date1.getString("jour") + date1.getString("mois") + date1.getString("annee"));
            }
        }
        Button b2 = (Button) findViewById(R.id.enregistrer);
        final FraisDB fraisDb = new FraisDB(this);
        fraisDb.open();
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = ((EditText)findViewById(R.id.type)).getText().toString();
                EditText edText = ((EditText)findViewById(R.id.type));
                edText.setEnabled(false);
                edText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                    @Override
                    public void onFocusChange(View v, boolean hasFocus){
                        if(!hasFocus){
                            hideKeyboard(getApplicationContext(), v);
                        }
                    }
                });
                String mont = ((EditText)findViewById(R.id.montant)).getText().toString();
                double m = Float.parseFloat(mont);
                String date = ((TextView)findViewById(R.id.dateChoi)).getText().toString();
                Frais frais = new Frais(type, m, date);
                long i = fraisDb.insertFrais(frais);
                fraisDb.close();
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Frais bien enregistrés", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
