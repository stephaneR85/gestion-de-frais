package com.example.stephanerafflin.gestiondefrais;

import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.DatePicker;

/**
 * Created by stephane.rafflin on 31/08/2017.
 */

public class dateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date);

        DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
        Calendar cal = Calendar.getInstance();
        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener(){
            @Override
            public void onDateChanged(DatePicker datePicker,int i, int i1, int i2){
                //Toast.makeText(getApplicationContext(),datePicker.getDayOfMonth()+"-"+datePicker.getMonth()+"-"+datePicker.getYear(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(dateActivity.this, MainActivity.class);
                intent.putExtra("jour", datePicker.getDayOfMonth()+" - ");
                intent.putExtra("mois", datePicker.getMonth()+1+" - ");
                intent.putExtra("annee", datePicker.getYear()+"");
                startActivity(intent);
            }
        });
    }
}
