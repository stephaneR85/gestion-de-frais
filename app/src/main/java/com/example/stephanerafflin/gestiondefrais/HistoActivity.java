package com.example.stephanerafflin.gestiondefrais;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stephane.rafflin on 18/09/2017.
 */

public class HistoActivity extends Activity {

    FraisDB db = new FraisDB(this);
    SimpleCursorAdapter adapter1;
    int idFrais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.historique);
        Bundle d = getIntent().getExtras();
        String date = d.getString("date");
        Button btnRet = (Button) findViewById(R.id.retourL);
        btnRet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HistoActivity.this, ListMonth.class);
                startActivity(intent);
            }
        });
        affiche(date);
    }
    public void affiche(String date) {
        ListView tab = (ListView) findViewById(R.id.list);
        db.open();
        String[] from = new String[]{"date","type","montant"};
        int[] to = new int[]{R.id.col1,R.id.col2,R.id.col3};
        adapter1 = new SimpleCursorAdapter(this, R.layout.item_view, db.getFrais(date), from, to, 0);
        tab.setAdapter(adapter1);
        tab.setOnItemClickListener(new ListView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> av, View v, int pos, final long id) {
                Cursor item = (Cursor) av.getItemAtPosition(pos);
                //Toast.makeText(getApplicationContext(), item.getString(0), pos).show();
                int idF = Integer.parseInt(item.getString(0));
                dialogue(idF).show();
            }
        });
        db.close();
    }

    public void supprime(int id){
        db.open();
        db.delete(id);
        db.close();
    }
    public Dialog dialogue(int id){
        Dialog d;
        idFrais = id;
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Suppression");
                alert.setMessage("T'es sûr de vouloir supprimer");
                alert.setPositiveButton("Supprimer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        supprime(idFrais);
                        Intent intent = new Intent(HistoActivity.this, ListMonth.class);
                        startActivity(intent);
                    }
                });
                /*alert.setNeutralButton("Modifier", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    modif(idFrais);
                    Intent intent = new Intent(HistoActivity.this, ListMonth.class);
                    startActivity(intent);
                    }
                });*/
                alert.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                d = alert.create();
        return d;
    }
}