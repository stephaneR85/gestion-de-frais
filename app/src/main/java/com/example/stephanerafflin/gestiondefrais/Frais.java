package com.example.stephanerafflin.gestiondefrais;

import java.util.Date;

/**
 * Created by stephane.rafflin on 11/09/2017.
 */

public class Frais {

    private int _id;
    private String type;
    private double montant;
    private String date;

    public Frais(String type, double montant, String date) {
        this.type = type;
        this.montant = montant;
        this.date = date;
    }

    public Frais(){

    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "[Frais] id : " + _id + " - type : " + type + ", Montant : "
                + montant + " - Date : " + date;
    }

}

